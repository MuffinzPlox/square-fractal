using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading.Tasks;

namespace PlzNo
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {


        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        //Texture2D grid;
        RenderTarget2D renderTarget;

        //camera stuff... dun dun dun
        Camera2d cam = new Camera2d();
        float zoomSpeed = 0.01f;
        float panSpeed = 1f;

        //drawing & layers
        Texture2D fractalImage;
        int count = 0;
        int drawSpeed = 4;

        TimeSpan newLayerAt;
        TimeSpan newLayerIn;

        //rotations
        bool rotating = false;
        float RotationAngle;
        TimeSpan rotateCounter;

        //debug, tells me how many squares are drawn when it hits a limit
        int squareCount = 0;
        float pixelLimit = 0.1f;


        //controls
        SpriteFont font;
        MouseState ms;
        MouseState lastMs;

        KeyboardState ks;
        KeyboardState lastKs;

        bool finishedGenerating = false;



        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            IsMouseVisible = true;


            //change screen size
            graphics.PreferredBackBufferWidth = 1600;
            graphics.PreferredBackBufferHeight = 960;
            graphics.ApplyChanges();

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //render target
            renderTarget = new RenderTarget2D(
                GraphicsDevice,
                GraphicsDevice.PresentationParameters.BackBufferWidth,
                GraphicsDevice.PresentationParameters.BackBufferHeight);

            //globals
            Global.ScreenSize = new Vector2(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
            Global.ScreenCenter = new Vector2(GraphicsDevice.Viewport.Width / 2, GraphicsDevice.Viewport.Height / 2);

            

            base.Initialize();

            //camera
            var form = (System.Windows.Forms.Form)System.Windows.Forms.Control.FromHandle(this.Window.Handle);
            form.Location = new System.Drawing.Point(0, 0);
            cam.Pos = Global.ScreenCenter;
        }

        private void Populate()
        {
            for (int i = 0; i < Global.AllSquares.Count; i++) //161
            {
                Global.AllSquares[i].Load();

                if (Global.AllSquares[Global.AllSquares.Count - 1].Width < pixelLimit) //stops adding new squares when width of a new square becomes less than 0.1 pixels
                {
                    squareCount = Global.AllSquares.Count;
                    break;
                }
            }
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {


            //Pre Square items
            fractalImage = Content.Load<Texture2D>("octagon");
            float scale = 0.3f;

            //add first square
            Global.AllSquares.Add(new Square(fractalImage, new Vector2(Global.ScreenCenter.X/* - ((temp.Width * scale) / 2)*/, Global.ScreenCenter.Y/* - ((temp.Height * scale) / 2)*/), scale, Square.Side.Source));
            //load rest of squares
            Populate();

            //fonts & buttons
            font = Content.Load<SpriteFont>("font");

            newLayerAt = TimeSpan.FromSeconds(0.1f);
            newLayerIn = TimeSpan.Zero;

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //mouse & updates

            lastKs = ks;
            ks = Keyboard.GetState();

            lastMs = ms;
            ms = Mouse.GetState();

            newLayerIn += gameTime.ElapsedGameTime;
            if (newLayerIn >= newLayerAt && Global.AllSquares[Global.AllSquares.Count - 1] != null && !Global.AllSquares[Global.AllSquares.Count - 1].Visible)
            {
                newLayerIn = TimeSpan.Zero;
                //draws down from 'count' number of squares and breaks when hits already visible item
                for (int i = count; i >= 0; i--)
                {
                    if (Global.AllSquares[i] != null)
                    {
                        if (Global.AllSquares[i].Visible)
                        {
                            break;
                        }
                        else
                        {
                            Global.AllSquares[i].Visible = true;
                        }
                    }
                }

                //counter goes up if last square hasn't been drawn
                if (!Global.AllSquares[Global.AllSquares.Count - 1].Visible)
                {
                    count += drawSpeed;
                    drawSpeed *= 3;
                    if (count > Global.AllSquares.Count - 1)
                    {
                        count = Global.AllSquares.Count - 1;
                    }
                }
                //Global.VisibleSquares = Global.AllSquares.Where(s => s != null && s.Visible);
            }

            if (Global.AllSquares[Global.AllSquares.Count - 1].Visible)
            {
                finishedGenerating = true;
            }


            //buttons


            //animate rotating
            if (rotating)
            {
                rotateCounter += gameTime.ElapsedGameTime;
                RotationAngle += rotateCounter.Seconds;
                for (int i = 0; i < Global.AllSquares.Count; i++)
                {
                    Global.AllSquares[i].Rotation = MathHelper.ToRadians(RotationAngle);
                }
            }

            //controls

            if (ks.IsKeyDown(Keys.R) && lastKs.IsKeyUp(Keys.R))
            {
                rotating = !rotating;
                rotateCounter = TimeSpan.Zero;
                RotationAngle = 0;
                foreach (Square s in Global.AllSquares)
                {
                    s.Rotation = 0;
                }
            }

            //panning
            if (ks.IsKeyDown(Keys.W))
            {
                cam.Move(new Vector2(0, -panSpeed));
            }
            if (ks.IsKeyDown(Keys.S))
            {
                cam.Move(new Vector2(0, panSpeed));
            }
            if (ks.IsKeyDown(Keys.A))
            {
                cam.Move(new Vector2(-panSpeed, 0));
            }
            if (ks.IsKeyDown(Keys.D))
            {
                cam.Move(new Vector2(panSpeed, 0));
            }


            //adding squares
            if (ks.IsKeyDown(Keys.Z) && lastKs.IsKeyUp(Keys.Z))
            {
                if (lastKs.IsKeyUp(Keys.Up))
                {
                    int max = Global.AllSquares.Count - 1;
                    Parallel.For(0, max, (i) =>
                    {
                        if (Global.AllSquares[i] != null)
                        {
                            Global.AllSquares[i].Load();
                        }
                    });

                    //for (int i = 0; i < max; i++)
                    //{
                    //    Global.AllSquares[i].Load();
                    //}
                }
            }

            //zooming
            if (ks.IsKeyDown(Keys.Up))
            {
                cam.Zoom += zoomSpeed;
                zoomSpeed *= 1.1f;

                panSpeed /= 1.04f;
            }

            if (ks.IsKeyDown(Keys.Down))
            {
                cam.Pos = Global.ScreenCenter;
                cam.Zoom = 1.0f;

                zoomSpeed = 0.01f;
                panSpeed = 1.0f;
            }

            base.Update(gameTime);
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            if (!finishedGenerating)
            {
               
                GraphicsDevice.SetRenderTarget(renderTarget);

                spriteBatch.Begin();

                GraphicsDevice.Clear(Color.CornflowerBlue);
                foreach (Square s in Global.AllSquares) //Global.VisibleSquares
                {
                    if (s != null)
                    {
                        s.Draw(spriteBatch);
                    }
                }

                spriteBatch.End();

                GraphicsDevice.SetRenderTarget(null);

            }
            spriteBatch.Begin(SpriteSortMode.Deferred,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    cam.GetTransformation(GraphicsDevice));





            spriteBatch.Draw(renderTarget, new Vector2(0, 0), Color.White);




            spriteBatch.DrawString(font, string.Format("X: {0}\nY: {1}\nSquareCount: {2}", ms.X, ms.Y, Global.AllSquares.Count), Vector2.Zero, Color.White);

            spriteBatch.End();


            base.Draw(gameTime);
        }
    }
}
