﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace PlzNo
{
    class Button
    {
        string text;
        SpriteFont font;
        Vector2 position;
        Rectangle hitbox;
        Texture2D image;

        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                hitbox.X = (int)position.X;
                hitbox.Y = (int)position.Y;
            }
        }

        public Rectangle Hitbox
        {
            get
            {
                return hitbox;
            }
            set
            {
                hitbox = value;
            }
        }

        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }

        public Button(SpriteFont Font, string Text, Vector2 Position, Texture2D Image)
        {
            text = Text;
            font = Font;
            position = Position;
            image = Image;
            hitbox = new Rectangle((int)position.X, (int)position.Y, (int)font.MeasureString(text).X, (int)font.MeasureString(text).Y);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(image, hitbox, Color.White);
            spriteBatch.DrawString(font, text, position, Color.Black);
        }


    }
}
