﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PlzNo
{
    public class Square
    {
        public enum Side
        {
            Source = 0,
            Right = 1,
            Left = 2,
            Top = 3,
            Bottom = 4
        }

        #region Variables

        Texture2D image;
        Vector2 position;
        Vector2 origin;
        Color tint;
        float scale;
        float rotation;
        Side mySide;

        bool filled = false;
        bool visible = false;
        bool drawn = false;

        #endregion Variables

        #region Properties

        public bool Drawn
        {
            get
            {
                return drawn;
            }
        }

        public float Width
        {
            get
            {
                return image.Width * scale;
            }
        }

        public float Height
        {
            get
            {
                return image.Height * scale;
            }
        }

        public Color Color
        {
            get
            {
                return tint;
            }
            set
            {
                tint = value;
            }
        }

        public float Rotation
        {
            get
            {
                return rotation;
            }
            set
            {
                rotation = value;
            }
        }

        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }

        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        public float X
        {
            get
            {
                return position.X;
            }
            set
            {
                position = new Vector2(value, position.Y);
            }
        }

        public float Y
        {
            get
            {
                return position.Y;
            }
            set
            {
                position = new Vector2(position.X, value);
            }
        }

        #endregion Properties

        public Square(Texture2D Image, Vector2 Position, float Scale, Side thisSide)
        {

            image = Image;
            position = Position;
            scale = Scale;
            origin = new Vector2(image.Width/2, image.Height/2);
            tint = new Color(Global.rando.Next(0, 255), Global.rando.Next(0, 255), Global.rando.Next(0, 255));
            mySide = thisSide;
            rotation = 0;
        }

        public void Load()
        {
            if (filled)
            {
                return;
            }

            Vector2 tempSize = new Vector2(image.Width * (scale / 2), image.Height * (scale / 2));

            if (mySide != Side.Left)
            {
                Square temp = new Square(image, new Vector2(position.X + ((image.Width*scale)/2) + tempSize.X/2, position.Y), scale / 2, Side.Right);

                Global.AllSquares.Add(temp);

            }
            
            if (mySide != Side.Right)
            {
                Square temp = new Square(image, new Vector2(position.X - ((image.Width*scale)/2) - tempSize.X/2, position.Y), scale / 2, Side.Left); //not perfect

                Global.AllSquares.Add(temp);
            }
            
            if (mySide != Side.Top)
            {
                Square temp = new Square(image, new Vector2(position.X, position.Y + ((image.Height*scale)/2) + tempSize.Y/2), scale / 2, Side.Bottom);

                Global.AllSquares.Add(temp);
            }
            
            if (mySide != Side.Bottom)
            {
                Square temp = new Square(image, new Vector2(position.X, position.Y - ((image.Height*scale)/2) - tempSize.Y/2), scale / 2, Side.Top); //not perfect

                Global.AllSquares.Add(temp);
            }
            

            filled = true;

        }



        public void Draw(SpriteBatch spriteBatch)
        {
            if (visible)
            {
                spriteBatch.Draw(image, position, null, tint, rotation, origin, scale, SpriteEffects.None, 0f);
                drawn = true;
            }
        }


    }
}
